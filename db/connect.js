const mongoose = require('mongoose')
const DBconnect = (url) => {
    return mongoose.connect(url,{
        useNewUrlParser:true,
        useUnifiedTopology:true,
    })
}
module.exports = DBconnect