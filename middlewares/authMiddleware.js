const User = require('../models/userModel')
const asyncHandler = require('express-async-handler')

const isAdmin = asyncHandler(async(req,res,next) => {
    const {email} = req.body
    const adminUser = await User.findOne({email})
    if(adminUser.role !== 'admin'){
        res.json({
            status :  true,
            msg : "you are not a admin user"
        })
    }else{
        next()
    }
})

module.exports = {isAdmin}