const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const UserSchema = new mongoose.Schema({
    fullName : {
        type : String,
        maxLength : 100,
        minLength : 3,
        required : true
    },
    email : {
        type : String,
        required :true,
        unique : true,
        trim : true,
    },
    password : {
        type : String,
        required : true,
    },
    token : {
        type : String,
    },
    mobile : {
        type : Number,
        minLength : 10
    },
    role : {
        type : String,
        default : 'user',
    },
    address : {
        type : String,
    },
    pan : {
        type : String,
    },
    aadhar : {
        type : String
    },
    account : {
        type : Number
    },
    ifsc : {
        type : String
    }
})

UserSchema.pre('save',async function(next){
    try{
       this.password = await bcrypt.hash(this.password,10)
    }catch(error){
      throw new Error('error')
    }
})



const User = mongoose.model("User",UserSchema)
module.exports = User
