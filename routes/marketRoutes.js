const express = require('express')
const { get } = require('mongoose')
const router = express.Router()

const  {getIntraDay,
getIntraDayExtended,
getDaily,
getDailyAdjusted,
getWeekly,
getWeeklyAdjusted,
getMonthly,
getMonthlyAdjusted,
getGlobalQuote,
getTickerSearch,
getGlobalMarketStatus} = require('./../controllers/marketController')


router.get('/intraday/:symbol/:interval',getIntraDay)
router.get('/search/:symbol',getTickerSearch)
router.get('/globalmarketstatus',getGlobalMarketStatus)
router.get('/globalquote/:symbol',getGlobalQuote)


router.get('/intradayentended/',getIntraDayExtended)
router.get('/daily/:symbol',getDaily)
router.get('/dailyadjusted/:symbol',getDailyAdjusted)
router.get('/weekly/:symbol',getWeekly)
router.get('/weeklyadjusted/:symbol',getWeeklyAdjusted)
router.get('/monthly/:symbol',getMonthly)
router.get('/monthlyadjusted/:symbol',getMonthlyAdjusted)



module.exports = router 