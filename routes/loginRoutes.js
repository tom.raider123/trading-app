const express = require('express')
const router = express.Router()
const {registerUser, userLogin} = require('./../controllers/userController')


router.post('/login',userLogin)
router.post('/register',registerUser)

module.exports = router 



