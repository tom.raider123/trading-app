const express = require('express')
const router  = express.Router()
const {
    getAllUser,
    getUser,
    updateUser,
    deleteUser,
    forgetPassword,
    logOut,
    updateProfile
} = require('../controllers/userController')

const isAdmin = require('../middlewares/authMiddleware')
const verifyToken = require('../middlewares/verifyToken')



router.get('/get-all-user',verifyToken,getAllUser)
router.get('/:id',verifyToken,getUser) 
router.put('/:id',verifyToken,updateUser)
router.delete('/:id',verifyToken,deleteUser)
router.post('/forget-password',forgetPassword)
router.get('/logout',verifyToken,logOut)
router.put('/:id',verifyToken,updateProfile) 

module.exports = router