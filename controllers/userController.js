const User = require('../models/userModel')
const bcrypt = require('bcrypt');
const  jwt  = require('jsonwebtoken');
const asyncHandler = require('express-async-handler')


const registerUser = asyncHandler (async(req,res) => {
    try{
        const { email } = req.body;
        const is_user_exist = await User.findOne({email :email})
        if(!is_user_exist){
        const user = await User.create(req.body);
        return res.json({status : true,msg : "user created successfully",data : user})
        }else{
            return res.json({status : false,msg : "user already exist"})
 
        }
    }catch(error){
        return res.json({status : false,msg : error})
    }
})

const getAllUser = asyncHandler(async(req,res) =>{
    try{
        const user = await User.find()
        return res.json({status : true,msg : "user received successfully",data : user})
    }catch(error){
        return res.json({status:false,msg:error})

    }
})

const getUser = asyncHandler(async(req,res) => {
    try{
    const user = await User.findById(req.params.id)
    return res.json({
        status: true,
        masg : "user received successfully",
        data: user
    })
}catch(error){
    return res.json({status:false,msg:error})

}
})

const updateUser = asyncHandler(async(req,res) => {
   try{
    const user = await User.findByIdAndUpdate(req.params.id,req.body,{new:true})
    return res.json({status:true, msg:"user updated", data:user})
   }catch(error){
    return res.json({status:false, msg:error})

   }
})

const deleteUser = asyncHandler(async (req,res) => {
    try{
        const user = await User.findByIdAndDelete(req.params.id)
        return res.json({status : true, msg: "deleted", data:[]})
    }catch(error){
        return res.json({status : false, msg: error})
    }
})

const userLogin = asyncHandler(async (req,res) => {
    try{
      
        const {email, password} = req.body;
        const user = await User.findOne({email});
        if(!user){return res.json({ststus : false,msg:"This email does not exist"})}
        const isMatch = await bcrypt.compare(password , user.password)
        if(!isMatch){ return res.json({status:false, msg:"Password is incorrect"})}
        const token = jwt.sign({id:user._id},process.env.ACCESS_TOKEN_SECRET);
        user.token = token;
        if(token){
            res.json({
                status : true,
                msg : "login success",
                data : user,
            });
        }else{
            res.json({
                status :false,
                msg : "something went wrong"
            });
        }
    }catch(error){
       res.json({
        status : false,
        msg : error
       });
    }
    })
    
    const forgetPassword = asyncHandler(async(req,res) => {
        try{
           const { email , password } = req.body
           const user = await User.findOne({email : email})
           if(!user) return res.json({status : false , msg : "user not found"})
           const new_password = await bcrypt.hash(password,10)
           const update = await User.findByIdAndUpdate({_id:user._id},{$set:{password:new_password}})
           return  res.json({status : true , msg : "profile updated successfully", data : update})

        }catch(error){
           return  res.json({status : false , msg : error})
        }
    })
    
    const logOut = asyncHandler(async(req,res) => {
        try{
             jwt.destroy(req.id);
        }catch(error){
         return res.json({status: false, msg: error })
        }
    })

    const updateProfile = asyncHandler(async(req,res) => {
        try{
           const user =  await User.findByIdAndUpdate(req.params.id,req.body,{new:true})
            return res.json({status:true, msg:"profile updated", data:user})

        }catch(error){
            return res.json({status: false, msg: error })
        }
    })

module.exports = {
    registerUser,
    getAllUser,
    getUser,
    updateUser,
    deleteUser,
    userLogin,
    forgetPassword,
    logOut,
    updateProfile
}