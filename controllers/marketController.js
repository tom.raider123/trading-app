const  request = require('request');
const axios = require('axios');
require('dotenv').config()

const getIntraDay = async(req,res) => {
    try{
        let query = 'TIME_SERIES_INTRADAY';
        let symbol = req.params.symbol;
        let intervel = req.params.interval;
        var url = `https://www.alphavantage.co/query?function=${query}&symbol=${symbol}&outputsize=full&interval=${intervel}&apikey=${process.env.MARKET_API_KEY}`;  

  let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        return res.json({status:false, msg:error })
    }
}

const getIntraDayExtended = async(req,res) => {
    try{
        // console.log(req.query)
        let funct = 'TIME_SERIES_INTRADAY_EXTENDED';
        let symbol = req.query.symbol;
        let intervel = req.query.interval;
        let slice = req.query.slice;
        var url = `https://www.alphavantage.co/query?function=${funct}&symbol=${symbol}&outputsize=full&interval=${intervel}&slice=${slice}&apikey=${process.env.MARKET_API_KEY}`;
        
  let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        return res.json({status:false, msg:error })
    }
}

const getDaily = async(req,res) => {
    try{
        let query = 'TIME_SERIES_DAILY';
        let symbol = req.params.symbol;
        var url = `https://www.alphavantage.co/query?function=${query}&symbol=${symbol}&outputsize=full&apikey=${process.env.MARKET_API_KEY}`;  

  let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        return res.json({status:false, msg:error })
    }
}

const getDailyAdjusted = async(req,res) => {
    try{
        let query = 'TIME_SERIES_DAILY_ADJUSTED';
        let symbol = req.params.symbol;
        var url = `https://www.alphavantage.co/query?function=${query}&symbol=${symbol}&outputsize=full&apikey=${process.env.MARKET_API_KEY}`;  

        let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        return res.json({status:false, msg:error })
    }
}

const getWeekly = async(req,res) => {
    try{
        let query = 'TIME_SERIES_WEEKLY';
        let symbol = req.params.symbol;
        var url = `https://www.alphavantage.co/query?function=${query}&symbol=${symbol}&outputsize=full&apikey=${process.env.MARKET_API_KEY}`;  
  let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        return res.json({status:false, msg:error })
    }
}


const getWeeklyAdjusted = async(req,res) => {
    try{
        let query = 'TIME_SERIES_WEEKLY_ADJUSTED';
        let symbol = req.params.symbol;
        var url = `https://www.alphavantage.co/query?function=${query}&symbol=${symbol}&outputsize=full&apikey=${process.env.MARKET_API_KEY}`;  
  let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        return res.json({status:false, msg:error })
    }
}

const getMonthly = async(req,res) => {
    try{
        let query = 'TIME_SERIES_MONTHLY';
        let symbol = req.params.symbol;
   
        var url = `https://www.alphavantage.co/query?function=${query}&symbol=${symbol}&outputsize=full&apikey=${process.env.MARKET_API_KEY}`;  
  let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        return res.json({status:false, msg:error })
    }
}

const getMonthlyAdjusted = async(req,res) => {
    try{
        let query = 'TIME_SERIES_MONTHLY_ADJUSTED';
        let symbol = req.params.symbol;
       
        var url = `https://www.alphavantage.co/query?function=${query}&symbol=${symbol}&outputsize=full&apikey=${process.env.MARKET_API_KEY}`;  
  let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        return res.json({status:false, msg:error })
    }
}

const getGlobalQuote = async(req,res) => {
    try{
        let query = 'GLOBAL_QUOTE';
        let symbol = req.params.symbol;
        var url = `https://www.alphavantage.co/query?function=${query}&symbol=${symbol}&outputsize=full&apikey=${process.env.MARKET_API_KEY}`;  
  let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        return res.json({status:false, msg:error })
    }
}

const getTickerSearch = async(req,res) => {
    try{
        let query = 'SYMBOL_SEARCH';
        let keyword = req.params.symbol;
        var url = `https://www.alphavantage.co/query?function=${query}&keywords=${keyword}&apikey=${process.env.MARKET_API_KEY}`;
        let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        console.log(error)
        return res.json({status:false, msg:error })
    }
}

const getGlobalMarketStatus = async(req,res) => {
    try{
        let query = 'MARKET_STATUS';
        var url = `https://www.alphavantage.co/query?function=${query}&apikey=${process.env.MARKET_API_KEY}`;
  let data =  await axios.get(url)
    if(data){
        res.json(data.data)
    }else{
        return res.json({status:false, msg:"No data received !"})
    }
    }catch(error){
        return res.json({status:false, msg:error })
    }
}

module.exports = { 
    getIntraDay,
    getIntraDayExtended,
    getDaily,
    getDailyAdjusted,
    getWeekly,
    getWeeklyAdjusted,
    getMonthly,
    getMonthlyAdjusted,
    getGlobalQuote,
    getTickerSearch,
    getGlobalMarketStatus
}

